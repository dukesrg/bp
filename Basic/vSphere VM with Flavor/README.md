# Облачная ВМ на vSphere, с указанием обобщённого типа (flavor)

Свойства ВМ включают поля:
 - `name` - имя виртуальной машины
 - `flavor` - обобщённый тип компьютерных ресурсов, привязанный к конкретному типу конкретной облачной среды
 - `imageRef` - [ссылка на образ](https://docs.vmware.com/en/vRealize-Automation/8.0/Using-and-Managing-Cloud-Assembly/GUID-9CBAA91A-FAAD-4409-AFFC-ACC1810E4FA5.html) из которого разворачивается виртуальная машина